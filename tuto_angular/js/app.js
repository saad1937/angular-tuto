var tutorial = angular.module('tutorial',['tutorial.controllers','ui.router']);

tutorial.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider){

	$urlRouterProvider.otherwise("/home");
	$stateProvider
	.state("utils",{
		abstract : true,
		views : {
			"header" : {
				templateUrl : 'partials/header.html',
				controller : 'HeadController',
				controllerAs : 'HeadCtrl'
			},
			"sidebar" : {
				templateUrl : 'partials/sidebar.html'
			}
		}
		
	})
	.state("home",{
		url : '/home',
		parent : 'utils',
		views : {
			"content@" : {
				templateUrl : 'partials/home.html'
			}
		}
		
	})
	.state("details",{
		url : '/details',
		parent : 'utils',
		views : {
			"content@" : {
				templateUrl : 'partials/details.html'
			}
		}
		
	});
}]);

